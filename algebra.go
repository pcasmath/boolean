// Algebra defines the interface for a boolean algebra.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package boolean

import (
	"bitbucket.org/pcasmath/object"
)

// Algebra is the interface satisfied by a boolean algebra.
type Algebra interface {
	object.Parent
	// True returns the value corresponding to true.
	True() object.Element
	// False returns the value corresponding to false.
	False() object.Element
	// IsTrue returns true iff x is equal to the value corresponding to true.
	IsTrue(x object.Element) (bool, error)
	// IsFalse returns true iff x is equal to the value corresponding to false.
	IsFalse(x object.Element) (bool, error)
	// And returns the result x AND y.
	And(x object.Element, y object.Element) (object.Element, error)
	// Or returns the result x OR y.
	Or(x object.Element, y object.Element) (object.Element, error)
	// Not returns the negation NOT x.
	Not(x object.Element) (object.Element, error)
}
