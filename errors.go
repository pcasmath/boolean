// Errors defines common errors.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package boolean

// objectError implements an error.
type objectError int

// Common errors.
const (
	ErrArgNotABoolean = objectError(iota)
	ErrArg1NotABoolean
	ErrArg2NotABoolean
)

/////////////////////////////////////////////////////////////////////////
// objectError functions
/////////////////////////////////////////////////////////////////////////

// Error returns the error message.
func (e objectError) Error() string {
	switch e {
	case ErrArgNotABoolean:
		return "argument is not a boolean"
	case ErrArg1NotABoolean:
		return "argument 1 is not a boolean"
	case ErrArg2NotABoolean:
		return "argument 2 is not a boolean"
	default:
		return "unknown error"
	}
}

// Is return true iff target is equal to e.
func (e objectError) Is(target error) bool {
	ee, ok := target.(objectError)
	return ok && ee == e
}
